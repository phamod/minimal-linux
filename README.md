# Minimal Linux
Project to create a minimal bootable linux distribution This was built from a dnf/yum distribution based linux. This was created from a fedora 35 linux usb drive installed over a laptops sata drive

## building the init process
the command to build the init process run from this directory `gcc -o init -static test.c`

## extra requirements for building boost
`dnf install -y gcc-c++`

## building boost
After installing run the following script it will automatically select the gcc tool with the c++ libraries.  
`./bootstrap.sh`  
After the b2 executable is made run the following.  
`./b2 threading=multi variant=release`  
This will create the library files to a directory called `<boost-version>/stage/` and the header files all exist in `<boost-version>/boost`

## extra requirements for building bazel build tool
`dnf install -y java-11-openjdk java-11-openjdk-devel`
make automake gcc gcc-c++ kernel-devel

## building bazel


find -print0 | cpio -0oH newc | gzip -9 > ../../initramfs.cpio.gz