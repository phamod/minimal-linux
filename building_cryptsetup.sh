#!/bin/bash


wget https://gitlab.com/cryptsetup/cryptsetup/-/archive/v2.4.2/cryptsetup-v2.4.2.tar.gz

tar xf cryptsetup-v2.4.2.tar.gz
pushd cryptsetup-v2.4.2
./autogen.sh
./configure
make

mkdir -p _install/{lib64,sbin}
cp .libs/cryptsetup _install/sbin/
cp /lib64/ld-linux-x86-64.so.2 _install/lib64/

for lib in $(ldd _install/sbin/cryptsetup | awk '{ print $3 }'); do 
    [ $lib ] && cp $lib _install/$lib
done

pushd _install
mkdir -p ../../build
tar cf ../../build/cryptsetup.tar *
popd
popd

rm -rf cryptsetup*
