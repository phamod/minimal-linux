#!/bin/bash

wget https://busybox.net/downloads/busybox-1.34.1.tar.bz2
tar xvf busybox-1.34.1.tar.bz2
pushd busybox-1.34.1
make defconfig

sed -i "s/# CONFIG_STATIC is not set/CONFIG_STATIC=y/g" .config
make && make install

# directory is used by cryptsetup when decrypting a drive
mkdir _install/run

echo "creating disk for root partition"
mkdir -p _install/etc/init.d

cat <<EOF > _install/etc/init.d/rcS
#!/bin/sh

mkdir -p /proc /sys /tmp /dev
mount -t tmpfs tmpfs /tmp
mount -a
mdev -s
EOF

chmod 755 _install/etc/init.d/rcS


cat <<EOF > _install/etc/fstab
/dev/sda3 / ext4 defaults 0 0
/dev/sda2 /boot ext4 defaults 0 0
/dev/sda1 /boot/efi vfat ro 0 0
EOF

pushd _install
tar cf ../../build/rootfs.tar *
popd

# TODO modify fstab to reflect install device UUIDs
echo "cleaning directories to create initramfs"
rm -rf _install/etc

# Get the kernel version from the tar ball this is needed when loading modules
kernel_version=$(ls ../build/ | grep "kernel-modules-" | sed "s/kernel-modules-//")

# generate folder for kernel modules to load
mkdir -p _install/lib/modules/${kernel_version}

# find all kernel modules and copy for initramfs to load
cp ../build/kernel-modules-${kernel_version}/* _install/lib/modules/${kernel_version}

# copy the init script that gets run by the kernel when loaded as initramfs
cp ../initramfs.init _install/init

pushd _install
# install cryptsetup for decryption
tar xf ../../build/cryptsetup.tar

# generate the initramfs file
find -print0 | cpio -0oH newc | gzip -9 > ../../build/initramfs.cpio.gz
popd 
popd

rm -rf busybox*