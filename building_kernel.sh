#!/bin/bash

wget https://www.kernel.org/pub/linux/kernel/v5.x/linux-5.13.12.tar.xz
kernel_zip=$(ls | grep "linux-.*.tar.xz")
kernel_dir=$(echo ${kernel_zip} | sed "s/.tar.xz//")
kernel_version=$(echo ${kernel_dir} | sed "s/linux-//")
tar xvf linux-5.13.12.tar.xz

pushd ${kernel_dir}
make mrproper
make defconfig

# provide configuration needed to generate the dm-crypt module needed for cryptsetup
sed -i "s/CONFIG_BLK_DEV_DM=y/CONFIG_BLK_DEV_DM=m/g" .config
sed -i "s/# CONFIG_DM_CRYPT is not set/CONFIG_DM_CRYPT=m/g" .config
sed -i "s/CONFIG_DM_MIRROR=y/CONFIG_DM_MIRROR=m/g" .config
sed -i "s/CONFIG_DM_ZERO=y/CONFIG_DM_ZERO=m/g" .config
sed -i "s/# CONFIG_CRYPTO_ECB is not set/CONFIG_CRYPTO_ECB=m/g" .config
sed -i "s/# CONFIG_CRYPTO_XTS is not set/CONFIG_CRYPTO_XTS=m/g" .config
sed -i "s/# CONFIG_CRYPTO_ESSIV is not set/CONFIG_CRYPTO_ESSIV=m/g" .config 
sed -i "s/# CONFIG_CRYPTO_TWOFISH is not set/# CONFIG_CRYPTO_TEA is not set\nCONFIG_CRYPTO_TWOFISH=m\nCONFIG_CRYPTO_TWOFISH_COMMON=m/g" .config
echo "CONFIG_CRYPTO_USER_API=m" >> .config
sed -i "s/# CONFIG_CRYPTO_USER_API_SKCIPHER is not set/CONFIG_CRYPTO_USER_API_SKCIPHER=m/g" .config 
echo "CONFIG_CRYPTO_USER_API_ENABLE_OBSOLETE=y" >> .config
echo "CONFIG_CRYPTO_ANUBIS=n" >> .config
echo "CONFIG_CRYPTO_ARC4=n" >> .config
echo "CONFIG_CRYPTO_KHAZAD=n" >> .config
echo "CONFIG_CRYPTO_SEED=n" >> .config

#config for kernel to run xen on x86_64
cat <<EOF >> .config
CONFIG_HYPERVISOR_GUEST=y
CONFIG_PARAVIRT=y
CONFIG_XEN=y
CONFIG_PARAVIRT_GUEST=y
CONFIG_PARAVIRT_SPINLOCKS=y
CONFIG_HVC_DRIVER=y
CONFIG_HVC_XEN=y
CONFIG_XEN_FBDEV_FRONTEND=y
CONFIG_XEN_BLKDEV_FRONTEND=y
CONFIG_XEN_NETDEV_FRONTEND=y
CONFIG_XEN_PCIDEV_FRONTEND=y
CONFIG_INPUT_XEN_KBDDEV_FRONTEND=y
CONFIG_XEN_FBDEV_FRONTEND=y
CONFIG_XEN_XENBUS_FRONTEND=y
CONFIG_XEN_SAVE_RESTORE=y
CONFIG_XEN_GRANT_DEV_ALLOC=m
CONFIG_XEN_TMEM=y
CONFIG_CLEANCACHE=y
CONFIG_FRONTSWAP=y
CONFIG_XEN_SELFBALLOONING=y
CONFIG_XEN_DOM0=y
CONFIG_XEN_DEV_EVTCHN=y
CONFIG_XENFS=y
CONFIG_XEN_COMPAT_XENFS=y
CONFIG_XEN_SYS_HYPERVISOR=y
CONFIG_XEN_GNTDEV=y
CONFIG_XEN_BACKEND=y
CONFIG_XEN_NETDEV_BACKEND=m
CONFIG_XEN_BLKDEV_BACKEND=m
CONFIG_XEN_BALLOON=y
CONFIG_XEN_SCRUB_PAGES=y
CONFIG_X86_IO_APIC=y
CONFIG_ACPI=y
CONFIG_ACPI_PROCFS=y
CONFIG_PCI_XEN=y
CONFIG_XEN_PCIDEV_BACKEND=m
EOF

make

cp arch/x86_64/boot/bzImage ../build/vmlinuz-${kernel_version}.scratch.x86_64
module_path="../build/kernel-modules-${kernel_version}"
mkdir -p ${module_path}
for mod in $(find ./ -name "*.ko"); do
    cp $mod ${module_path}
done
popd

# TODO collect all modules generated to a tarball based on the version and the bzimage of the kernel 

#rm -rf linux*