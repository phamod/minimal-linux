#!/bin/bash

[ $(command -v dnf) ] && dnf install -y bison flex ncurses-devel perl-Pod-Html openssl-libs openssl-devel elfutils-libelf-devel glibc-static 
[ $(command -v apt) ] && apt install -y bison flex ncurses-dev

# commands to build cryptsetup
[ $(command -v dnf) ] && dnf install -y libtool gettext-devel libuuid-devel popt-devel json-c-devel libssh-devel libblkid-devel