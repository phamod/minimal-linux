#!/bin/bash

set -e

MOUNT_POINT=/data
device=$(cat device)

echo "label: gpt
device: ${device}
unit: sectors

# made using a 16gb flash drive will try different partitioning to make work
1: size=600MiB,type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B,name=part-efi
2: size=1GiB,type=0FC63DAF-8483-4772-8E79-3D69D8477DE4,name=part-boot
3: size=12.7GiB,type=0FC63DAF-8483-4772-8E79-3D69D8477DE4,name=part-root" \
| sudo sfdisk ${device}

echo Y | sudo mkfs.fat ${device}1
echo Y | sudo mkfs.ext4 ${device}2

read -p "Do you want the drive encrypted? y to encrypt (n): " encrypt

if [ "${encrypt}" = "y" ]; then
    password="changeit"
    while [ "${password}" != "${confirm}" ]; do
        read -s -p "Please enter a password: " password
        read -s -p "Please confirm password: " confirm
    done
    echo -e "${password}\n${password}" | sudo cryptsetup luksFormat ${device}3
    echo "${password}" | sudo cryptsetup luksOpen ${device}3 system
    sudo mkdir -p ${MOUNT_POINT}
    echo Y | sudo mkfs.ext4 /dev/mapper/system 
    sudo mount /dev/mapper/system ${MOUNT_POINT}    
else 
    echo Y | sudo mkfs.ext4 ${device}3 
    sudo mkdir -p ${MOUNT_POINT}
    sudo mount ${device}3 ${MOUNT_POINT}
fi


sudo mkdir -p ${MOUNT_POINT}/boot
sudo mount ${device}2 ${MOUNT_POINT}/boot
sudo mkdir -p ${MOUNT_POINT}/boot/efi
sudo mount ${device}1 ${MOUNT_POINT}/boot/efi


wget https://ftp.gnu.org/gnu/grub/grub-2.06.tar.xz
tar xf grub-2.06.tar.xz 
pushd grub-2.06/
./configure --prefix=${MOUNT_POINT}/usr          \
            --sysconfdir=${MOUNT_POINT}/etc      \
            --target=x86_64 \
            --with-platform=efi \
            --disable-efiemu       \
            --disable-werror

make && sudo make install

popd

# trying out
sudo ${MOUNT_POINT}/usr/sbin/grub-install --recheck --no-floppy --root-directory=${MOUNT_POINT} ${device}

rm -rf grub-2.06*

sudo tar xf build/rootfs.tar -C ${MOUNT_POINT}
sudo cp -rf build/initramfs.cpio.gz ${MOUNT_POINT}/boot/
sudo cp -rf build/vmlinuz-*.scratch.x86_64 ${MOUNT_POINT}/boot
sudo cp -rf grub.cfg ${MOUNT_POINT}/boot/grub/

partition=$(echo ${device} | sed "s|/dev/||")
uuid=$(lsblk -o NAME,UUID | grep -i ${partition}1 | awk '{print $2}')
sed -i "s|/dev/sda1|UUID=${uuid}|" ${MOUNT_POINT}/etc/fstab

uuid=$(lsblk -o NAME,UUID | grep -i ${partition}2 | awk '{print $2}')
sed -i "s|/dev/sda2|UUID=${uuid}|" ${MOUNT_POINT}/etc/fstab
sudo sed -i "s|/dev/sda2|UUID=${uuid}|" ${MOUNT_POINT}/boot/grub/grub.cfg

if [ "${encrypt}" = "y" ]; then
    #TODO provides steps for automating encrypted uuid
    echo "needs work"
else 
    uuid=$(lsblk -o NAME,UUID | grep -i ${partition}3 | awk '{print $2}')
    sed -i "s|/dev/sda3|UUID=${uuid}|" ${MOUNT_POINT}/etc/fstab
    sudo sed -i "s|/dev/sda3|UUID=${uuid}|" ${MOUNT_POINT}/boot/grub/grub.cfg
fi

sudo umount ${device}1
sudo umount ${device}2
# TODO use cryptsetup to close if encrypted to unmount
sudo umount ${device}3
